require('dotenv/config');

const Database = require('../../../../../components/database');
const mailer = require('../../../../../components/mailer');
const Cryptr = require('cryptr');
const database = new Database();
const cryptr = new Cryptr(process.env.AUTH_SECRET_KEY);

module.exports = async ( req, res ) => {

    const { ianemail } = req.body;

    await database.Query(process.env.QUERY_RECOVER_ACCOUNT, [ianemail, ianemail], (results, error) => {
        if (error) console.log(error);
        else {
            if (typeof results === 'undefined' || results.length === 0) res.json({ error: 1 });
            else {
                if (results[0]['ACTION'] == 'v') res.json({ error: 2 });
                else {
                    const id = results[0]['ID'];
                    const email = results[0]['EMAIL'];
                    let date = Date.now();
                    date += 30 * 60000;
                    const exptoken = cryptr.encrypt(date);
                    database.Query(process.env.QUERY_RECOVER_TOKENS, [id, exptoken, 'p'], (result, err) => {
                        if (err) res.json({ database: err });
                        else {
                            mailer.Send(mailer.Options({
                                to: email,
                                subject: 'Lowkey - Recuperar a Palavra-Passe',
                                html: `<a href="http://lvh.me/conta/recuperar/${exptoken}"> Clique Aqui</a><br>`
                            }));
                            res.json({ success: email });
                        }
                    });
                }
            }
        }
    });
}