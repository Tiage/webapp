/*
 * Project: Lowkey
 * Source: ~main.js
 *
 *  @see electron
 *
 *  @const BrowserWindow: required electron module
 *  @const app: required electron module
 *
 *  @var mainWindow : BrowserWindow | null
 *
 *  Author: Tiago Miguel
 */

const { BrowserWindow, app } = require('electron')

/*
 * Attention
 *
 * Comment out `require('./server.js')`
 * when using the following commands:
 *  @command: npm run compile-package-mac
 *  @command: npm run compile-package-win@ia32
 *  @command: npm run compile-package-win@x64
 *  @command: npm run compile-package-linux
 *
 *  @see electron-packager: documentation
 */
require('./server.js')

let mainWindow = null

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    minHeight: 750,
    minWidth: 750,
    webPreferences: {
      nodeIntegration: false,
      nodeIntegrationInWorker: false
    }
  })
  mainWindow.loadURL('http://lvh.me/build-dist')
  mainWindow.setMenuBarVisibility(false)
  mainWindow.on('close', event => (mainWindow = null))
})
