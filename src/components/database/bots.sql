USE STORAGE;
INSERT INTO `accounts` (`ID`, `IAN`, `NAME`, `EMAIL`, `PASSWORD`, `CREATED`) VALUES
(1, 'admin', 'Administrador', 'admin@lowkey.com', '$2a$10$pxGvJLbVVYgIbaDbRM.qSePA/68REsX/vPp8BZIxOu/JyBflw8rMG', '2019-02-27 13:50:23'),
(2, 'tiago', 'Tiago Miguel', 'bot_1@lowkey.com', '$2a$10$Vf5Iu3ebx4gHBi0u5Lb7/Om1iVKsFbSJ0Q7ZiAKwg7BDs.4UikLnG', '2019-02-27 13:50:23'),
(3, 'bot_2', 'BOT_2', 'bot_2@lowkey.com', '$2a$10$Vf5Iu3ebx4gHBi0u5Lb7/Om1iVKsFbSJ0Q7ZiAKwg7BDs.4UikLnG', '2019-02-27 13:50:23'),
(4, 'bot_3', 'BOT_3', 'bot_3@lowkey.com', '$2a$10$Vf5Iu3ebx4gHBi0u5Lb7/Om1iVKsFbSJ0Q7ZiAKwg7BDs.4UikLnG', '2019-02-27 13:50:23'),
(5, 'bot_4', 'BOT_4', 'bot_4@lowkey.com', '$2a$10$Vf5Iu3ebx4gHBi0u5Lb7/Om1iVKsFbSJ0Q7ZiAKwg7BDs.4UikLnG', '2019-02-27 13:50:23');

INSERT INTO `managers` (`USER`) VALUES (1);

INSERT INTO `friends` (`ME`, `FRIEND`, `STATUS`, `TOKEN`, `ACTION`, `SINCE`) VALUES
(1, 2, '2', '$2a$10$ZNVnxuCY0XEldegAckKSgeOuXhySNHnWck0BO/jeV2GUqTH8r5jHa', 1, '2019-03-21 12:29:23'),
(1, 3, '2', '$2a$10$LHnYdoA6Rw4KWsA14ryf6uUeeUwayxQRsTavfci8m16SWzB2qJD0i', 1, '2019-03-19 17:08:00'),
(1, 4, '2', '$2a$10$Mfd4haOvIkBsYgJanuGNfeDrKjrbvZ2pZTP8AVtFtRWSPhC9SQu1i', 1, '2019-03-22 12:38:17'),
(1, 5, '2', '$2a$10$HkWPT2C9pzOOpsGAk2L4nus92bQ6EzCXm88/8fV4ekt8QPtBDqrsO', 1, '2019-03-22 12:11:06'),
(2, 1, '2', '$2a$10$ZNVnxuCY0XEldegAckKSgeOuXhySNHnWck0BO/jeV2GUqTH8r5jHa', 1, '2019-03-21 12:29:23'),
(3, 1, '2', '$2a$10$LHnYdoA6Rw4KWsA14ryf6uUeeUwayxQRsTavfci8m16SWzB2qJD0i', 1, '2019-03-19 17:08:00'),
(4, 1, '2', '$2a$10$Mfd4haOvIkBsYgJanuGNfeDrKjrbvZ2pZTP8AVtFtRWSPhC9SQu1i', 1, '2019-03-22 12:38:17'),
(5, 1, '2', '$2a$10$HkWPT2C9pzOOpsGAk2L4nus92bQ6EzCXm88/8fV4ekt8QPtBDqrsO', 1, '2019-03-22 12:11:06');

INSERT INTO `messages` (`ID`, `SENDER`, `MESSAGE`, `TOKEN`, `DATE`, `EDITED`, `VISIBLE`) VALUES
(1, 1, 'First Message', '$2a$10$ZNVnxuCY0XEldegAckKSgeOuXhySNHnWck0BO/jeV2GUqTH8r5jHa', '2019-03-22 19:36:10', 0, 1),
(2, 2, 'yooooo', '$2a$10$ZNVnxuCY0XEldegAckKSgeOuXhySNHnWck0BO/jeV2GUqTH8r5jHa', '2019-03-22 19:44:11', 0, 1);
