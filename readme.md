# LOWKEY 💬

## Instalação
 
Para iniciar o projeto no seu computador, precisa de ter instalado: o [NodeJS](https://nodejs.org/en/) - Importante, visto que o projeto é baseado nesta framework!

Depois de instalado, é necessário instalar o "Nodemon", para reiniciar a aplicação, caso encontre uma alteração nova no código fonte.

Verifique se está a usar a versão mais recente do NodeJS: node -v
Para Instalar o "Nodemon", na linha de comandos: npm i -g nodemon

## Desenvolvido com:
* [NodeJS](https://nodejs.org/) - Usado como servidor (backend)

#### Modulos Usados
* [ExpressJS](https://expressjs.com/) - Minimal and flexible Node.js web application framework
* [BCryptJS](https://github.com/dcodeIO/bcrypt.js) - Secure and optimized password hashing module
* [Socket.io](https://github.com/socketio/socket.io) - Socket.io enables real-time bidirectional event-based communication
* [Morgan](https://github.com/expressjs/morgan) - Small express HTTP logger for debugging asynchronous requests and responses
* [DotEnv](https://github.com/motdotla/dotenv) - Dotenv is a zero-dependency module that loads environment variables

## Autor
* [Tiago Miguel](https://gitlab.com/TiagoMiguel)