/*
 * Project: Lowkey
 * Source: ~/components/mailer
 *
 *  @see dotenv/config: ~.env
 *  @see nodemailer
 *
 *  @const nodemailer: required nodemailer module
 *  @const transporter: nodemailer/transport
 *      @var service : String
 *      @var auth    : Object
 *          @var user : String
 *          @var pass : String
 *
 *  @method: Options
 *      @args data: Object
 *          @var from    : String
 *          @var to      : String
 *          @var subject : String
 *          @var html    : String
 *
 *  @method: Send
 *      @see nodemailer/sendMail
 *
 *      @args method: Options
 *      @method: sendMail
 *          @args options  : ...Options Object
 *          @args callback : Function
 *              @method: callback
 *                  @var error   : Object
 *                  @var success : Object
 *
 *  Author: Tiago Miguel
 */

require('dotenv/config')

const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
  service: process.env.MAILER_SERVICE,
  auth: {
    user: process.env.MAILER_AUTH_USER,
    pass: process.env.MAILER_AUTH_PASS
  }
})

const Options = data => ({
  from: process.env.MAILER_AUTH_USER,
  to: data.to,
  subject: data.subject,
  html: data.html
})

const Send = options =>
  transporter.sendMail(options, (error, sucess) => {
    if (error) console.log(error)
    else console.log('Email sent: ' + sucess.response)
  })

module.exports.Options = Options
module.exports.Send = Send
